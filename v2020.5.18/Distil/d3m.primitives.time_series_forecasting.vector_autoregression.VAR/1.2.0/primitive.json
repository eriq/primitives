{
    "id": "76b5a479-c209-4d94-92b5-7eba7a4d4499",
    "version": "1.2.0",
    "name": "VAR",
    "keywords": [
        "Time Series"
    ],
    "source": {
        "name": "Distil",
        "contact": "mailto:jeffrey.gleason@kungfu.ai",
        "uris": [
            "https://github.com/kungfuai/d3m-primitives"
        ]
    },
    "installation": [
        {
            "type": "PIP",
            "package": "cython",
            "version": "0.29.16"
        },
        {
            "type": "PIP",
            "package_uri": "git+https://github.com/kungfuai/d3m-primitives.git@985acacd7e7eaf592f4ed5de480a37d42706a084#egg=kf-d3m-primitives"
        }
    ],
    "python_path": "d3m.primitives.time_series_forecasting.vector_autoregression.VAR",
    "algorithm_types": [
        "VECTOR_AUTOREGRESSION"
    ],
    "primitive_family": "TIME_SERIES_FORECASTING",
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "primitives.ts_forecasting.vector_autoregression.var.VarPrimitive",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m.container.pandas.DataFrame",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Params": "primitives.ts_forecasting.vector_autoregression.var.Params",
            "Hyperparams": "primitives.ts_forecasting.vector_autoregression.var.Hyperparams"
        },
        "interfaces_version": "2020.5.18",
        "interfaces": [
            "supervised_learning.SupervisedLearnerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "max_lag_order": {
                "type": "d3m.metadata.hyperparams.Union",
                "default": 1,
                "structural_type": "typing.Union[NoneType, int]",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "The lag order to apply to regressions. If user-selected, the same lag will be applied to all regressions. If auto-selected, different lags can be selected for different regressions.",
                "configuration": {
                    "user_selected": {
                        "type": "d3m.metadata.hyperparams.UniformInt",
                        "default": 1,
                        "structural_type": "int",
                        "semantic_types": [],
                        "lower": 0,
                        "upper": 100,
                        "lower_inclusive": true,
                        "upper_inclusive": false
                    },
                    "auto_selected": {
                        "type": "d3m.metadata.hyperparams.Hyperparameter",
                        "default": null,
                        "structural_type": "NoneType",
                        "semantic_types": [],
                        "description": "Lag order of regressions automatically selected"
                    }
                }
            },
            "default_lag_order": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 1,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "default lag order to use if matrix decomposition errors",
                "lower": 0,
                "upper": 100,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "seasonal": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": true,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "whether to perform ARIMA prediction with seasonal component"
            },
            "seasonal_differencing": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 1,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "period of seasonal differencing to use in ARIMA prediction",
                "lower": 1,
                "upper": 365,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "dynamic": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": true,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "whether to perform dynamic in-sample prediction with ARIMA model"
            },
            "interpret_value": {
                "type": "d3m.metadata.hyperparams.Enumeration",
                "default": "lag_order",
                "structural_type": "str",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "whether to return weight coefficients for each series or each lag order separately in the regression",
                "values": [
                    "series",
                    "lag_order"
                ]
            },
            "interpret_pooling": {
                "type": "d3m.metadata.hyperparams.Enumeration",
                "default": "avg",
                "structural_type": "str",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "whether to pool weight coefficients via average or max",
                "values": [
                    "avg",
                    "max"
                ]
            },
            "confidence_interval_horizon": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 2,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "horizon for confidence interval forecasts. Exposed through auxiliary 'produce_confidence_intervals' method",
                "lower": 1,
                "upper": 100,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "confidence_interval_alpha": {
                "type": "d3m.metadata.hyperparams.Uniform",
                "default": 0.1,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "significance level for confidence interval, i.e. alpha = 0.05 returns a 95%% confdience interval from alpha / 2 to 1 - (alpha / 2) . Exposed through auxiliary 'produce_confidence_intervals' method",
                "lower": 0.01,
                "upper": 1,
                "lower_inclusive": true,
                "upper_inclusive": false
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "primitives.ts_forecasting.vector_autoregression.var.Hyperparams",
                "kind": "RUNTIME"
            },
            "random_seed": {
                "type": "int",
                "kind": "RUNTIME",
                "default": 0
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "outputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "params": {
                "type": "primitives.ts_forecasting.vector_autoregression.var.Params",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {},
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams",
                    "random_seed"
                ],
                "returns": "NoneType"
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
                "description": "If there are multiple endogenous series, primitive will fit VAR model. Otherwise it will fit an ARIMA\nmodel. In the VAR case, the lag order will be automatically choosen based on BIC (unless user overrides).\nIn the ARIMA case, the lag order will be automatically chosen by differencing tests (again, unless user\noverrides).\n\nKeyword Arguments:\n    timeout {float} -- timeout, not considered (default: {None})\n    iterations {int} -- iterations, not considered (default: {None})\n\nReturns:\n    CallResult[None]\n\nParameters\n----------\ntimeout:\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "outputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nThis method allows primitive author to implement an optimized version of both fitting\nand producing a primitive on same data.\n\nIf any additional method arguments are added to primitive's ``set_training_data`` method\nor produce method(s), or removed from them, they have to be added to or removed from this\nmethod as well. This method should accept an union of all arguments accepted by primitive's\n``set_training_data`` method and produce method(s) and then use them accordingly when\ncomputing results.\n\nThe default implementation of this method just calls first ``set_training_data`` method,\n``fit`` method, and all produce methods listed in ``produce_methods`` in order and is\npotentially inefficient.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to ``set_training_data`` and all produce methods.\noutputs:\n    The outputs given to ``set_training_data``.\ntimeout:\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "primitives.ts_forecasting.vector_autoregression.var.Params",
                "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nAn instance of parameters."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to all produce methods.\ntimeout:\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "prediction for future time series data\n\nArguments:\n    inputs {Inputs} -- full D3M dataframe, containing attributes, key, and target\n\nKeyword Arguments:\n    timeout {float} -- timeout, not considered (default: {None})\n    iterations {int} -- iterations, not considered (default: {None})\n\nRaises:\n    PrimitiveNotFittedError: if primitive not fit\n\nReturns:\n    CallResult[Outputs] -- (N, 2) dataframe with d3m_index and value for each prediction slice requested.\n        prediction slice = specific horizon idx for specific series in specific regression\n\nParameters\n----------\ninputs:\n    The inputs of shape [num_inputs, ...].\ntimeout:\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nThe outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
            },
            "produce_confidence_intervals": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "produce confidence intervals for each series 'confidence_interval_horizon' periods into \n        the future\n\nArguments:\n    inputs {Inputs} -- full D3M dataframe, containing attributes, key, and target\n\nKeyword Arguments:\n    timeout {float} -- timeout, not considered (default: {None})\n    iterations {int} -- iterations, considered (default: {None})\n\nRaises:\n    PrimitiveNotFittedError: \n\nReturns:\n    CallResult[Outputs] -- \n\n    Ex. \n        series | timestep | mean | 0.05 | 0.95\n        --------------------------------------\n        a      |    0     |  5   |   3  |   7\n        a      |    1     |  6   |   4  |   8\n        b      |    0     |  5   |   3  |   7\n        b      |    1     |  6   |   4  |   8"
            },
            "produce_weights": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Produce absolute values of correlation coefficients (weights) for each of the terms used in each regression model. \n    Terms must be aggregated by series or by lag order (thus the need for absolute value). Pooling operation can be maximum \n    or average (controlled by 'interpret_pooling' HP).\n\nArguments:\n    inputs {Inputs} -- full D3M dataframe, containing attributes, key, and target\n\nKeyword Arguments:\n    timeout {float} -- timeout, not considered (default: {None})\n    iterations {int} -- iterations, considered (default: {None})\n\nRaises:\n    PrimitiveNotFittedError: if primitive not fit\n\nReturns:\n    CallResult[Outputs] -- pandas df where each row represents a unique series from one of the regressions that was fit. \n    The columns contain the coefficients for each term in the regression, potentially aggregated by series or lag order. \n    Column names will represent the lag order or series to which that column refers. \n    If the regression is an ARIMA model, the set of column names will also contain AR_i (autoregressive terms) and \n        MA_i (moving average terms)\n    Columns that are not included in the regression for a specific series will have NaN values in those\n        respective columns. "
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams:\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [
                    "inputs",
                    "outputs"
                ],
                "returns": "NoneType",
                "description": "Sets primitive's training data\n\nArguments:\n    inputs {Inputs} -- D3M dataframe containing attributes\n    outputs {Outputs} -- D3M dataframe containing targets\n\nRaises:\n    ValueError: If multiple columns are annotated with 'Time' or 'DateTime' metadata\n\nParameters\n----------\ninputs:\n    The inputs.\noutputs:\n    The outputs."
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        },
        "params": {
            "integer_time": "bool",
            "time_column": "str",
            "key": "typing.List[int]",
            "targets": "typing.List[str]",
            "target_indices": "typing.List[int]",
            "target_types": "typing.List[str]",
            "X_train": "typing.Union[typing.List[d3m.container.pandas.DataFrame], typing.List[pandas.core.frame.DataFrame]]",
            "X_train_names": "typing.Any",
            "filter_idxs": "typing.List[str]",
            "interpolation_ranges": "typing.Union[NoneType, pandas.core.frame.DataFrame, pandas.core.series.Series]",
            "freq": "str",
            "is_fit": "bool",
            "fits": "typing.Union[typing.List[primitives.ts_forecasting.utils.arima.Arima], typing.List[statsmodels.tsa.vector_ar.var_model.VARResultsWrapper], typing.List[typing.Union[primitives.ts_forecasting.utils.arima.Arima, statsmodels.tsa.vector_ar.var_model.VARResultsWrapper]]]",
            "values": "typing.List[numpy.ndarray]",
            "values_diff": "typing.List[numpy.ndarray]",
            "lag_order": "typing.Union[typing.List[NoneType], typing.List[int], typing.List[numpy.int64], typing.List[typing.Union[NoneType, int, numpy.int64]], typing.List[typing.Union[NoneType, int]], typing.List[typing.Union[NoneType, numpy.int64]]]",
            "positive": "typing.List[bool]"
        }
    },
    "structural_type": "primitives.ts_forecasting.vector_autoregression.var.VarPrimitive",
    "description": "Primitive that applies a VAR multivariate forecasting model to time series data. The VAR\nimplementation comes from the statsmodels library. It will default to an ARIMA model if\ntimeseries is univariate. The lag order and AR, MA, and differencing terms for the VAR\nand ARIMA models respectively are selected automatically and independently for each regression.\nUser can override automatic selection with 'max_lag_order' HP.\n\nArguments:\n    hyperparams {Hyperparams} -- D3M Hyperparameter object\n\nKeyword Arguments:\n    random_seed {int} -- random seed (default: {0})\n\nAttributes\n----------\nmetadata:\n    Primitive's metadata. Available as a class attribute.\nlogger:\n    Primitive's logger. Available as a class attribute.\nhyperparams:\n    Hyperparams passed to the constructor.\nrandom_seed:\n    Random seed passed to the constructor.\ndocker_containers:\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes:\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory:\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "digest": "5a26ce129569401f5f6aeab13089bad3701e21626dc7251e986bc47a55ec962e"
}
